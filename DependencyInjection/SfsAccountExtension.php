<?php

namespace Softspring\AccountBundle\DependencyInjection;

use Softspring\AccountBundle\Entity\Account\Account as AccountDefaultEntity;
use Softspring\AccountBundle\Entity\Relation\AccountUserRelation as AccountUserRelationDefaultEntity;
use Softspring\AccountBundle\Entity\User\User as UserDefaultEntity;
use Softspring\AccountBundle\Model\Account as AccountModel;
use Softspring\AccountBundle\Model\AccountInterface;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class SfsAccountExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $processor = new Processor();
        $configuration = new Configuration();

        $config = $processor->processConfiguration($configuration, $configs);

        $container->setParameter('sfs_account.account.class', $config['entities']['account']);
        $container->setParameter('sfs_account.relation.class', $config['entities']['relation']);
        $container->setParameter('sfs_account.user.class', $config['entities']['user']);

        // load default mappings
        if ($config['entities']['account'] === AccountDefaultEntity::class) {
            $container->setParameter('sfs_account.load_account_default_mapping', true);
        }
        if ($config['entities']['relation'] === AccountUserRelationDefaultEntity::class) {
            $container->setParameter('sfs_account.load_relation_default_mapping', true);
        }
        if ($config['entities']['user'] === UserDefaultEntity::class) {
            $container->setParameter('sfs_account.load_user_default_mapping', true);
        }

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yaml');
    }

    public function prepend(ContainerBuilder $container)
    {
        $doctrineConfig = [];

        // add a default config to force load target_entities, will be overwritten by ResolveDoctrineTargetEntityPass
        $doctrineConfig['orm']['resolve_target_entities'][AccountInterface::class] = AccountModel::class;

        // disable auto-mapping for this bundle to prevent mapping errors
        $doctrineConfig['orm']['mappings']['SfsAccountBundle'] = [
            'is_bundle' => true,
            'mapping' => false,
        ];

        $container->prependExtensionConfig('doctrine', $doctrineConfig);
    }
}
