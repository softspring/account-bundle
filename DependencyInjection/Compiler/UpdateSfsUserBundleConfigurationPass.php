<?php

namespace Softspring\AccountBundle\DependencyInjection\Compiler;

use Softspring\AccountBundle\Entity\User\User as SfsAccountBundleUserEntity;
use Softspring\UserBundle\Entity\Base\User as SfsUserBundleUserEntity;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class UpdateSfsUserBundleConfigurationPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // set config parameters
        $sfsUserBundleUserClass = $container->getParameter('sfs_user.user.class');
        $sfsAccountBundleUserClass = $container->getParameter('sfs_account.user.class');

        if ($sfsUserBundleUserClass == SfsUserBundleUserEntity::class && $sfsAccountBundleUserClass == SfsAccountBundleUserEntity::class) {
            $container->setParameter('sfs_user.user.class', $sfsAccountBundleUserClass);
            $container->getParameterBag()->remove('sfs_user.user.load_user_default_mapping', false);
        }
    }
}