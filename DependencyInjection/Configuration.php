<?php

namespace Softspring\AccountBundle\DependencyInjection;

use Softspring\AccountBundle\Entity\Account\Account as AccountDefaultEntity;
use Softspring\AccountBundle\Entity\Relation\AccountUserRelation as AccountUserRelationDefaultEntity;
use Softspring\AccountBundle\Entity\User\User as UserDefaultEntity;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('sfs_account');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->arrayNode('entities')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('account')->defaultValue(AccountDefaultEntity::class)->end()
                        ->scalarNode('relation')->defaultValue(AccountUserRelationDefaultEntity::class)->end()
                        ->scalarNode('user')->defaultValue(UserDefaultEntity::class)->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }

}