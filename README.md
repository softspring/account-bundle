# Account bundle

### Configure ORM

#### Account entity

    namespace App\Entity;
    
    use Doctrine\ORM\Mapping as ORM;
    use Softspring\AccountBundle\Model\Account as SfsAccount;
    
    /**
     * @ORM\Entity()
     * @ORM\Table(name="account")
     */
    class Account extends SfsAccount
    {
        /**
         * @var string
         * @ORM\Column(type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;
    }

#### Users single accounted

**Account entity**

    /**
     * @var ArrayCollection|User[]
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="account")
     */
    protected $users;
    
**User entity account**
    
    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Softspring\AccountBundle\Model\AccountedUserInterface;
    use Softspring\AccountBundle\Model\AccountedUserTrait;
    use Softspring\AccountBundle\Model\AccountInterface;
    use Softspring\UserBundle\Model\User as SfsUser;

    class User extends SfsUser implements AccountedUserInterface
    {
        use AccountedUserTrait;
        
        /**
         * @var AccountInterface|null
         * @ORM\ManyToOne(targetEntity="App\Entity\Account", inversedBy="users")
         */
        protected $account;
    }
    

#### Users multiple accounted

**Account entity**

    /**
     * @var ArrayCollection|User[]
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="accounts")
     * @ORM\JoinTable(name="account_user",
     *      joinColumns={@ORM\JoinColumn(name="account_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    protected $users;
    
**User entity account**
    
    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Softspring\AccountBundle\Model\AccountedUserInterface;
    use Softspring\AccountBundle\Model\AccountInterface;
    use Softspring\AccountBundle\Model\MultiAccountedUserTrait;
    use Softspring\UserBundle\Model\User as SfsUser;

    class User extends SfsUser implements AccountedUserInterface
    {
        use MultiAccountedUserTrait;
        
        /**
         * @var AccountInterface|null
         * @ORM\ManyToMany(targetEntity="App\Entity\Account", inversedBy="users")
         */
        protected $accounts;
    }
    