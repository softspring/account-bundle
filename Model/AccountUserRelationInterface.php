<?php

namespace Softspring\AccountBundle\Model;

interface AccountUserRelationInterface
{
    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface;

    /**
     * @return AccountInterface
     */
    public function getAccount(): AccountInterface;

    /**
     * @param UserInterface $user
     */
    public function setUser(UserInterface $user): void;

    /**
     * @param AccountInterface $account
     */
    public function setAccount(AccountInterface $account): void;
}