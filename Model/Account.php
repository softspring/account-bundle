<?php

namespace Softspring\AccountBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Account implements AccountInterface
{
    /**
     * @var string|null
     */
    protected $id;

    /**
     * @var UserInterface|null
     */
    protected $owner;

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var Collection|UserInterface[]
     */
    protected $userRelations;

    /**
     * Account constructor.
     */
    public function __construct()
    {
        $this->userRelations = new ArrayCollection();
    }

    /**
     * @inheritdoc
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    /**
     * @inheritdoc
     */
    public function getOwner(): ?UserInterface
    {
        return $this->owner;
    }

    /**
     * @inheritdoc
     */
    public function setOwner(?UserInterface $owner): void
    {
        $this->owner = $owner;
    }

    /**
     * @inheritdoc
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @inheritdoc
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @inheritdoc
     */
    public function getUserRelations(): Collection
    {
        return $this->userRelations;
    }

    /**
     * @inheritdoc
     */
    public function addUserRelation(AccountUserRelationInterface $relation): void
    {
        if (!$this->userRelations->contains($relation)) {
            $this->userRelations->add($relation);
        }
    }

    /**
     * @inheritdoc
     */
    public function removeUserRelation(AccountUserRelationInterface $relation): void
    {
        if ($this->userRelations->contains($relation)) {
            $this->userRelations->removeElement($relation);
        }
    }

    /**
     * @inheritdoc
     */
    public function getUsers(): Collection
    {
        return $this->userRelations->map(function(AccountUserRelationInterface $relation) {
            return $relation->getUser();
        });
    }
}