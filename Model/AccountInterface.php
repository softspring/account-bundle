<?php

namespace Softspring\AccountBundle\Model;

use Doctrine\Common\Collections\Collection;

interface AccountInterface
{
    /**
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * @param string|null $id
     */
    public function setId(?string $id): void;

    /**
     * @return UserInterface|null
     */
    public function getOwner(): ?UserInterface;

    /**
     * @param UserInterface|null $owner
     */
    public function setOwner(?UserInterface $owner): void;

    /**
     * @return null|string
     */
    public function getName(): ?string;

    /**
     * @param null|string $name
     */
    public function setName(?string $name): void;

    /**
     * @return Collection|AccountUserRelationInterface[]
     */
    public function getUserRelations(): Collection;

    /**
     * @param AccountUserRelationInterface $relation
     */
    public function addUserRelation(AccountUserRelationInterface $relation): void;

    /**
     * @param AccountUserRelationInterface $relation
     */
    public function removeUserRelation(AccountUserRelationInterface $relation): void;

    /**
     * @return UserInterface[]|Collection
     */
    public function getUsers(): Collection;
}