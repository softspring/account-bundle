<?php

namespace Softspring\AccountBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Softspring\UserBundle\Model\User as BaseUser;

class User extends BaseUser implements UserInterface
{
    /**
     * @var Collection|UserInterface[]
     */
    protected $accountRelations;

    /**
     * UserAccounted constructor.
     */
    public function __construct()
    {
        $this->accountRelations = new ArrayCollection();
        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    public function getAccountRelations(): Collection
    {
        return $this->accountRelations;
    }

    /**
     * @inheritdoc
     */
    public function addAccountRelation(?AccountUserRelationInterface $relation): void
    {
        if (!$this->accountRelations->contains($relation)) {
            $this->accountRelations->add($relation);
        }
    }

    /**
     * @inheritdoc
     */
    public function removeAccountRelation(?AccountUserRelationInterface $relation): void
    {
        if ($this->accountRelations->contains($relation)) {
            $this->accountRelations->removeElement($relation);
        }
    }

    /**
     * @inheritdoc
     */
    public function getAccounts(): Collection
    {
        return $this->accountRelations->map(function(AccountUserRelationInterface $relation) {
            return $relation->getAccount();
        });
    }
}