<?php

namespace Softspring\AccountBundle\Model;

use Doctrine\Common\Collections\Collection;
use Softspring\UserBundle\Model\UserInterface as BaseUserInterface;

interface UserInterface extends BaseUserInterface
{
    /**
     * @return AccountUserRelationInterface[]|Collection
     */
    public function getAccountRelations(): Collection;

    /**
     * @param AccountUserRelationInterface|null $relation
     */
    public function addAccountRelation(?AccountUserRelationInterface $relation): void;

    /**
     * @param AccountUserRelationInterface|null $relation
     */
    public function removeAccountRelation(?AccountUserRelationInterface $relation): void;

    /**
     * @return AccountInterface[]|Collection
     */
    public function getAccounts(): Collection;
}