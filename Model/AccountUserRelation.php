<?php

namespace Softspring\AccountBundle\Model;

class AccountUserRelation implements AccountUserRelationInterface
{
    /**
     * @var AccountInterface
     */
    protected $account;

    /**
     * @var UserInterface
     */
    protected $user;

    /**
     * @return AccountInterface
     */
    public function getAccount(): AccountInterface
    {
        return $this->account;
    }

    /**
     * @param AccountInterface $account
     */
    public function setAccount(AccountInterface $account): void
    {
        $this->account = $account;
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * @param UserInterface $user
     */
    public function setUser(UserInterface $user): void
    {
        $this->user = $user;
    }
}