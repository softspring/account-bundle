<?php

namespace Softspring\AccountBundle;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Softspring\AccountBundle\DependencyInjection\Compiler\ResolveDoctrineTargetEntityPass;
use Softspring\AccountBundle\DependencyInjection\Compiler\UpdateSfsUserBundleConfigurationPass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SfsAccountBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $basePath = realpath(__DIR__.'/Resources/config/doctrine-mapping/');

        $this->addRegisterMappingsPass($container, [$basePath.'/entities/account' => 'Softspring\AccountBundle\Entity\Account'], 'sfs_account.load_account_default_mapping');
        $this->addRegisterMappingsPass($container, [$basePath.'/entities/relation' => 'Softspring\AccountBundle\Entity\Relation'], 'sfs_account.load_relation_default_mapping');
        $this->addRegisterMappingsPass($container, [$basePath.'/entities/user' => 'Softspring\AccountBundle\Entity\User'], 'sfs_account.load_user_default_mapping');
        $this->addRegisterMappingsPass($container, [$basePath.'/model' => 'Softspring\AccountBundle\Model']);

        $container->addCompilerPass(new ResolveDoctrineTargetEntityPass());
        $container->addCompilerPass(new UpdateSfsUserBundleConfigurationPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 1);
    }

    /**
     * @param ContainerBuilder $container
     * @param array            $mappings
     * @param string|bool      $enablingParameter
     */
    private function addRegisterMappingsPass(ContainerBuilder $container, array $mappings, $enablingParameter = false)
    {
        $container->addCompilerPass(DoctrineOrmMappingsPass::createXmlMappingDriver($mappings, ['sfs_account_em'], $enablingParameter));
    }
}